# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Added

- feat: add certbot_port variable
- feat: add hook scripts
- test: add support debian 12

### Changed

- test: use personal docker registry

## v2.2.0 - 2021-08-24

### Added

- test: add check yamllint
- test: add support debian 11

### Changed

- test: replace kitchen to molecule
- chore: use FQCN for module name
- feat: check if the port 80 is used

### Removed

- test: remove support debian 9

## v2.1.1 - 2018-11-26

- fix: replace shell module to command
- test: add check ansible-lint with galaxy rules

## v2.1.0 - 2018-11-25

- BREAKING CHANGE: minimal ansible version is 2.5 now
- fix: replace inline module to cron for renew cron
- test: use new docker images
- test: add tavis-ci to run tests

## v2.0.0 - 2018-07-07

- feat: add renew hook script

## v1.0.0 - 2018-06-10

- first version
