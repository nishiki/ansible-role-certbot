# Ansible role: Certbot

[![Version](https://img.shields.io/badge/latest_version-2.2.0-green.svg)](https://code.waks.be/nishiki/ansible-role-certbot/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-certbot/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-certbot/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-certbot/actions?workflow=molecule.yml)

Generate certificate SSL with certbot.

## Requirements

- Ansible >= 2.10
- Debian
  - Bullseye
  - Bookworm

## Role variables

- `certbot_mail` - mail address used by let's encrypt to notify
- `certbot_key_size` - private key size (default: `4096`)
- `certbot_port` - port to listen for certbot web (default: `80`)
- `certbot_path` - path where certbot write temporary files(default: `/var/www/acme`)
- `certbot_domains` - dict with the domain name and the script

```
  website.com:
    #!/bin/bash
    echo "test" > /tmp/log
```

- `certbot_role` - string must be master or slave, if master generate the certificates

## How to use

```
- hosts: git-server
  roles:
    - certbot
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-plugins[docker] docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2018 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
